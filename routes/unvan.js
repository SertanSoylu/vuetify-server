var express = require('express');
var router = express.Router();
var db = require('../app-settings');
var app = require('../app');
 

const sel = "select u.* from unvan u where (1=1) ";
const ins = "insert into unvan (unvan_id, unvan_adi, aktif, tarih) values ($1, $2, $3, $4) ";
const del = "delete from unvan where (unvan_id=$1) "
const orderBy = " order by unvan_id ";

/* GET unvan listing. */
router.get('/', function (req, res, next) {
  db.any(sel+orderBy, [])
    .then(function (data) {
      res.json(data);
    })
    .catch(function (error) {
      res.json(error);
    })
    //.finally(db.$pool.end);
});

/* GET unvan listing. */
router.get('/:id', function (req, res, next) {
  db.any(sel + 'and (unvan_id=$1) ' + orderBy, [req.params.id])
    .then(function (data) {
      res.json(data);
    })
    .catch(function (error) {
      res.json(error);
    })
    //.finally(db.$pool.end);
});


/* POST unvan adding. */
router.post('/', function (req, res, next) {
  var unvan = req.body;
  var seq_id = 0;
  console.log(unvan);


  db.any('select max(u.unvan_id) as max_id from unvan u',[])
  .then(function (data) {
    seq_id = data[0].max_id; 
    seq_id++;
    console.log("seq_id: "+seq_id);

    db.any(ins, [seq_id, unvan.unvan_adi, unvan.aktif, unvan.tarih])
    .then(function (data) {
      console.log(data);
      res.redirect(`/unvan/${seq_id}`);
    })
    .catch(function (error) {
      res.json(error);
    });
  })
  .catch(function (error) {
    res.json(error);
  })
  //.finally(db.$pool.end);

});

/* PUT unvan updating. */
router.put('/:id', function (req, res, next) {
  var unvan = req.body;
  console.log(unvan);

  db.any('update unvan set unvan_adi=$1, aktif=$2, tarih=$3 where unvan_id=$4',[unvan.unvan_adi, unvan.aktif, unvan.tarih,req.params.id])
  .then(function (data) {
    res.json(data);
  })
  .catch(function (error) {
    res.json(error);
  })
  //.finally(db.$pool.end);

});

/* DELETE unvan deleting. */
router.delete('/:id', function (req, res, next) {
  var unvan = req.body;
  console.log(unvan);

  db.any(del,[req.params.id])
  .then(function (data) {
    res.json(data);
  })
  .catch(function (error) {
    res.json(error);
  })
  //.finally(db.$pool.end);

});

module.exports = router;

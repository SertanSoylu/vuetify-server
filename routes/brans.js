var express = require('express');
var router = express.Router();
var db = require('../app-settings');

var qoper = 'select b.* from brans b where (1=1) ';


/* GET brans listing. */
router.get('/', function(req, res, next) {
  db.any(qoper)
    .then(function (data) {
      //console.log('DATA:', data)
      res.json(data);
     })
    .catch(function (error) {
      //console.log('ERROR:', error)
      res.json({
	"errorcode": error.code,
	"errormessage" : error.message,
      });
    })  

});

/* GET unvan listing. */
router.get('/:id', function(req, res, next) {
  db.any(qoper +'and (brans_id=$1) ', [req.params.id])
    .then(function (data) {
      //console.log('DATA:', data)
      res.json(data);
     })
    .catch(function (error) {
      //console.log('ERROR:', error)
      res.json({
	"errorcode": error.code,
	"errormessage" : error.message,
      });
    })  

    //console.log(db);

});

module.exports = router;

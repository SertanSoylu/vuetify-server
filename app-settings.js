/*
select = db.any
insert = db.none
insert with result = db.one
function call db.func('myFuncName', [123, new Date()])
db.proc('myProcName', [123, new Date()])
db.result = raw
*/


const pgp = require('pg-promise')(/*options*/);

const dbconn = {
		provider: "postgres",
		user: "postgres",
		pass: "dya123yeny",
		host: "127.0.0.1",
		port: "5432",
		database: "EGEREF", // sensitive
};

const connstr = dbconn.provider+"://"+
			  dbconn.user+":"+dbconn.pass+"@"+
			  dbconn.host+":"+dbconn.port+"/"+
			  dbconn.database;	


const db = pgp(connstr);

module.exports = db;				  